package maze.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import maze.logic.Maze;

import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;


/**
 * 
 * Classe que representa a Janel para Criar um Novo Labirinto do Modo Grafico
 * 
 * @author Andre Duarte 
 * @author Sergio Esteves 
 *
 */
public class FormMaze extends JFrame {

	private static boolean positionelements=false;
	private static char[][] campo;
	private static final long serialVersionUID = 1L;
	private static int dragonsnumber;
	private static int numberexits=0;
	private static int numberheroes=0;
	private static int numberspades=0;
	private static boolean possible=false;
	private JPanel contentPane;
	private Form parent;
	private static JPanel panel;
	private static int xdimension;
	
	






/**
	 * 
	 * Retorna o numero de dragoes
	 * 
	 * @return numero de dragoes
	 */

	public int getDragonsnumber() {
		return dragonsnumber;
	}

	/**
	 * 
	 * Set Numero de Dragoes
	 * 
	 * @param dragonsnumber
	 */
	public void setDragonsnumber(int dragonsnumber) {
		FormMaze.dragonsnumber = dragonsnumber;
	}







	/**
	*
	*@return campo Retorna o campo escolhido pelo utilizador
	*
	*/
	public char[][] getCampo() {
		return campo;
	}

	/**
	*
	*@param campo Atribui o campo escolhido pelo utilizador
	*
	*/
	public void setCampo(char[][] campo) {
		FormMaze.campo = campo;
	}


	/**
	*
	*
	*Get da variavel possible
	*
	*@return possible true se o labirinto e possivel e falso se nao
	*
	*/
	public boolean isPossible() {
		return possible;
	}

	/**
	*
	*
	*Set da variavel possible
	*
	*@param possible true se o labirinto e possivel e falso se nao
	*
	*/
	public void setPossible(boolean possible) {
		FormMaze.possible = possible;
	}




	/**
	 * Create the frame.
	 */
	public FormMaze(Form parent,int x,int y) {
		xdimension=x;
		dragonsnumber=y;

		this.parent=parent;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		panel = new JPanel();
		panel.setLayout(new GridLayout(xdimension,xdimension));
		contentPane.add(panel, BorderLayout.CENTER);


		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout(0,1));
		contentPane.add(panel2, BorderLayout.SOUTH);


		final JButton button_Seguinte=new JButton("Seguinte");
		button_Seguinte.setFocusable(false);

		panel2.add(button_Seguinte);

		button_Seguinte.addMouseListener(new MouseAdapter() {
			@Override

			public void mouseClicked(MouseEvent arg0) {
				if(!positionelements)
				{
					if(numberexits==1)
					{
						char[][] campo= new char[xdimension][xdimension];
						for(int i=0;i<xdimension;i++)
						{
							for(int j=0;j<xdimension;j++)
							{
								campo[i][j]=(((JButton)panel.getComponent(i*xdimension+j)).getText().charAt(0));
								//System.out.println(campo[i][j]);
							}
						}

						Maze teste = new Maze();
						teste.setCampo(campo);
						if(teste.verificaCaminhos())
						{
							System.out.println("Deu");
							JOptionPane.showMessageDialog(null, "Labirinto V�lido, Coloque as figuras");
							positionelements=true;

						}
						else
						{
							System.out.println("N�o deu");
							JOptionPane.showMessageDialog(null, "Labirinto N�o V�lido");

						}



					}
				}
				else
				{
					if(numberheroes==1 && numberspades==1)
					{
						campo= new char[xdimension][xdimension];
						JOptionPane.showMessageDialog(null, "Labirinto Criado com Sucesso");
						//Contar o numero de dragoes
						dragonsnumber=0;
						for(int i=0;i<xdimension;i++)
						{
							for(int j=0;j<xdimension;j++)
							{
								campo[i][j]=(((JButton)panel.getComponent(i*xdimension+j)).getText().charAt(0));
								if(((JButton)panel.getComponent(i*xdimension+j)).getText().charAt(0)=='D')
								{
									dragonsnumber++;
								}
								//System.out.println(campo[i][j]);
							}
						}	
						//PERGUNTAR
						//Chamar para jogar
closing();					}
					else
					{
						JOptionPane.showMessageDialog(null, "Tem de conter apenas um her�i e uma espada");
					}
					//Testar se � valido dragoes + heroi
				}
			}
		});
		for(int i=0;i<xdimension;i++)
		{
			for(int j=0;j<xdimension;j++)
			{
				final JButton lbl1=new JButton("X");
				lbl1.setSize(50,50);
				lbl1.setBorder( BorderFactory.createLineBorder(Color.BLACK));
				panel.add(lbl1);
				if((i==0 && j>0 && j<xdimension-1) || (i==xdimension-1 && j>0 && j<xdimension-1) || (i>0 && j==0 && i<xdimension-1) ||(i>0 && j==xdimension-1 && i<xdimension-1))
				{
					lbl1.addMouseListener(new MouseAdapter() {
						@Override

						public void mouseClicked(MouseEvent arg0) {
							if(!positionelements)
							{
								if(lbl1.getText()=="X" && numberexits==0)
								{
									lbl1.setText("S");
									numberexits=1;
								}
								else if(lbl1.getText()=="S")
								{
									lbl1.setText("X");
									numberexits=0;
								}
							}
						}
					});

				}
				else
				{
					lbl1.addMouseListener(new MouseAdapter() {
						@Override

						public void mouseClicked(MouseEvent arg0) {
							if(!positionelements)
							{
								if(lbl1.getText()=="X")
								{
									lbl1.setText(" ");

								}
								else
								{
									lbl1.setText("X");

								}
							}
							else
							{
								if(lbl1.getText()==" ")
								{
									if(numberheroes==0)
									{
										lbl1.setText("H");
										numberheroes++;
									}
									else if(numberspades==0)
									{
										lbl1.setText("E");
										numberspades++;
									}
									else
									{
										lbl1.setText("D");
									}
								}
								else if(lbl1.getText()=="H")
								{
									if(numberspades==0)
									{
										lbl1.setText("E");
										numberspades++;
									}
									else
									{
										lbl1.setText("D");
									}
									numberheroes--;
								}
								else if(lbl1.getText()=="E")
								{

									lbl1.setText("D");

									numberspades--;
								}
								else if(lbl1.getText()=="D")
								{
									lbl1.setText(" ");

								}
							}
						}

					});
				}
				lbl1.setFocusable(false);
			}
		}
	}

	/**
	* Funcao para fechar a janela
	*/
	public void closing()
	{
		this.setVisible(false);
		parent.setXdimension(xdimension);
		parent.setDragonsnumber(dragonsnumber);
		parent.setCampo(campo);
		parent.getFrame().setVisible(true);
		parent.creategame();
	}

}
