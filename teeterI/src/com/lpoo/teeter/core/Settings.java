package com.lpoo.teeter.core;

/**
 * The type Settings.
 */
public class Settings {

    private static Settings singletonObject;

    private Settings() {
        //	 Optional Code
    }

    /**
     * Get settings.
     *
     * @return the settings
     */
    public static synchronized Settings get() {
        if (singletonObject == null) {
            singletonObject = new Settings();
        }
        return singletonObject;
    }
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    /**
     * The FPS.
     */
    public final int FPS = 60;

    /**
     * The WIDTH.
     */
    public final int WIDTH = 960;
    /**
     * The HEIGHT.
     */
    public final int HEIGHT = 540;

    /**
     * The GRID_SIZE.
     * The GRID_SIZE.
     */
    public final int GRID_SIZE = 30;

    /**
     * The BALL_THRESHOLD.
     */
    public final double BALL_THRESHOLD = 0.1;
}
