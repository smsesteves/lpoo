package maze.test;

import static org.junit.Assert.*;

import maze.logic.Game;

import org.junit.Test;

public class eagle {
	
	char [][] campo10={	{'X','X','X','X','X'},
						{'X',' ',' ',' ','X'},
						{'X','D',' ','E','X'},
						{'X',' ',' ','H','X'},
						{'X','X','X','S','X'}};
	
	char [][] campo15={	{'X','X','X','X','X'},
						{'X','E',' ',' ','X'},
						{'X','D',' ',' ','X'},
						{'X',' ',' ','H','X'},
						{'X','X','X','S','X'}};
	
	@Test 
	public void testEagle() {
		Game g1=new Game(campo10,0);
		g1.play('A', 1);
		assertEquals(g1.getCampoAguia()[2][3], 'a');
		g1.play('D', 0);
		assertEquals(g1.getCampo()[3][3], 'A');
	}
	
	@Test
	public void testEagleDead() {
		Game g1=new Game(campo15,0);
		g1.play('A', 1);
		assertEquals(g1.getCampoAguia()[2][2], 'a');
		g1.play('D', 1);
		assertEquals(g1.getCampoAguia()[1][1], ' ');

	}

}
