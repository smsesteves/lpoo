package maze.cli;


import java.io.FileNotFoundException;
import java.util.Scanner;


import maze.logic.Game;
import maze.logic.Maze;

/**
 * 
 * Classe Main modo Consola
 * 
 * @author Andre Duarte 
 * @author Sergio Esteves
 *
 */
public class Main {
	
	static Game jogo;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Maze maze = new Maze();
		
		Scanner s = new Scanner(System.in);
		
		// Numero de lados
		System.out.print("N�mero de Linhas: ");
		int n = s.nextInt();	
		maze.generate(n);
		
		// Numero de Drag�es
		System.out.print("N�mero de Drag�es: ");
		n = s.nextInt();
		maze.positioningElements(n);
		
		// Tipo de Jogo
		System.out.print("Tipo de Jogo: ");
		n = s.nextInt();
		
		jogo = new Game(maze.getCampo(), n);
		
		//jogo.save();
		//jogo.load();
		
		
		while(true) {
			
			draw();
			System.out.print("Direc��o de Movimento: ");
			char dir = readInput();
			
			int eagleHelp;
			
			if(jogo.isEagleAbleToHelp()) {
				System.out.print("Queres ajuda da �guia (0-N�o/1-Sim): ");
				eagleHelp = s.nextInt();
			} else {
				eagleHelp = 0;
			}
			
			try {
				if(!jogo.play(dir,eagleHelp))
					break;
			} catch(IllegalArgumentException e) {
				System.out.println("Direc��o Inv�lida.");
				continue;
			}
			
			
		}
		draw();
		return;
	}

	private static void draw()
	{
		char [][] campo = jogo.getCampo();
		char [][] campoAguia = jogo.getCampoAguia();
		
		for(int i=0;i<campo.length;i++)
		{
			for(int j=0;j<campo.length;j++)
			{
				System.out.print(campo[i][j]);
				System.out.print(campoAguia[i][j]);
			}
			System.out.print("\n");
		}
	}
	
	private static char readInput()
	{
		Scanner s = new Scanner(System.in);
		String op = s.nextLine();
		if(op.length() != 1)
			return '�';
		if(op.equals("a") || op.equals("A") )
			return Maze.cHeroArmed;
		if(op.equals("w") || op.equals("W"))
			return 'W';
		if(op.equals("d") || op.equals("D"))
			return 'D';
		if(op.equals("s") || op.equals("S"))
			return 'S';
		return '�';
	}

}
