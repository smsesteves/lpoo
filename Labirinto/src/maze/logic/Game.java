package maze.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.io.*;


/**
 * 
 * Classe que implementa a l�gica de jogo
 * 
 * @author Andre Duarte
 * @author Sergio Esteves
 *
 */
public class Game {

	char [][] campo = {	{'X','X','X','X','X','X','X','X','X','X'},
			{'X','H',' ',' ',' ',' ',' ',' ',' ','X'},
			{'X',' ','X','X',' ','X',' ','X',' ','X'},
			{'X','D','X','X',' ','X',' ','X',' ','X'},
			{'X',' ','X','X',' ','X',' ','X',' ','X'},
			{'X',' ',' ',' ',' ',' ',' ',' ',' ','S'},
			{'X',' ','X','X',' ','X',' ','X',' ','X'},
			{'X',' ','X','X',' ','X',' ','X',' ','X'},
			{'X','E','X','X',' ',' ',' ',' ',' ','X'},
			{'X','X','X','X','X','X','X','X','X','X'}};
	
	char [][] campoAguia;

	Dragon[] dragons;

	Eagle eagle = new Eagle();

	int gameMode;
	Hero hero = new Hero();

	Spade spade = new Spade();

	/**
	 * Construtor por defeito
	 */
	public Game() {
		
	}

	/**
	 * Contrutor para optar se se usa ou n�o o labirinto por defeito
	 * 
	 * @param defaultMaze
	 */
	public Game(boolean defaultMaze, int GameMode) {
		
		gameMode = GameMode;
		
		if(defaultMaze) {
			recognizeElements();
		}		
		
		
	}

	/**
	 * Construtor para iniciar jogo com campo e modo de jogo definido
	 *  
	 * @param Campo
	 * @param GameMode
	 */
	public Game(char[][] Campo, int GameMode) {
		campo = Campo;
		gameMode = GameMode;		

		recognizeElements();		
	}

	private boolean checkBorders(GameElement object, GameElement attacker) {
		// Adjacente
		int diffX = Math.abs(attacker.getX() - object.getX());
		int diffY = Math.abs(attacker.getY() - object.getY());

		if((diffX == 1 && diffY == 0) || (diffY == 1 && diffX == 0) || (diffY == 0 && diffX == 0))
			return false;
		return true;
	}

	private boolean checkDeaths() {

		if(dragons.length > 0) {
			for(Dragon dragon : dragons) {

				if(dragon.isDead() || dragon.isSleepy())
					continue;

				// Aguia - Morre?
				if(!eagle.isFlying()) {
					if(!checkBorders(eagle, dragon))
						eagle.setEnd(true);
				}

				// Heroi - Morre?
				if(!spade.isCollected()) {
					if(!checkBorders(hero, dragon))
						return false;
				} else {
					if(!checkBorders(dragon, hero)) {
						dragon.setDead(true);
					}
				}
			}
		}
		
		removeDeadDragons();

		return true;
	}

	private void checkSpade() {
		if(spade.isCollected() || spade.isEagleCollected())
			return;

		if(spade.getX() == hero.getX() && spade.getY() == hero.getY())
		{
			eagle.setDead(true);
			spade.setCollected(true);
			campo[hero.getX()][hero.getY()] = Maze.cHeroArmed;
		}
	}	

	/**
	 * Retorna o campo
	 * 
	 * @return campo
	 */
	public char[][] getCampo() {
		return campo;
	}
	
	/**
	 * Retorna o campo da �guia
	 * 
	 * @return campoAguia
	 */
	public char[][] getCampoAguia() {
		return campoAguia;
	}
	
	/**
	 * Retorna a �guia
	 * 
	 * @return eagle
	 */
	public Eagle getEagle() {
		return eagle;
	}

	/**
	 * Retorna o Her�i
	 * 
	 * @return hero
	 */
	public Hero getHero() {
		return hero;
	}

	/**
	 * Retorna a espada
	 * 
	 * @return spade
	 */
	public Spade getSpade() {
		return spade;
	}

	/**
	 * @return true se a �guia est� dispon�vel para ajudar e false no caso contr�rio
	 */
	public boolean isEagleAbleToHelp() {
		return (!eagle.isLaunched() && !eagle.isEnd() && !spade.isCollected());
	}

	/**
	 * Carrega jogo do ficheiro
	 * 
	 * @return true se occorer com sucesso, false em caso contr�rio
	 */
	public boolean load() {
		
		try{

			ObjectInputStream save = new ObjectInputStream(new FileInputStream("saveFile.sav"));

			gameMode = (int) save.readObject();
			campo = (char[][]) save.readObject();
			campoAguia = (char[][]) save.readObject();
			hero = (Hero) save.readObject();
			spade = (Spade) save.readObject();
			eagle = (Eagle) save.readObject();
			dragons = (Dragon[]) save.readObject();

			save.close();

		} catch(Exception e) {
			return false;
		}
		
		recognizeElements();

		return true;

	}

	// auxX e auxY - Se menor que -1 ou maior que 1 � random
	// auxS - -1 = Random; 0 = N�o adormecer; 1 - Adormecer
	private void moveDragons(int auxY, int auxX, int auxS) {
		Random r = new Random();

		if(gameMode >= 1) {
			for(Dragon dragon : dragons) {
				if(dragon.isMoving() && dragon.isDead()==false)
				{					
					if(auxX < -1 || auxX > 1 || auxY < -1 || auxY > 1) {
						auxX = r.nextInt(3)-1;
						auxY = 0;
						if(auxX == 0)
							auxY = r.nextInt(3)-1;
					}
					if(campo[dragon.getX() + auxX][dragon.getY() + auxY] != Maze.cWall && campo[dragon.getX() + auxX][dragon.getY() + auxY] != 'S') {

						if(campo[dragon.getX()][dragon.getY()] != Maze.cDragonSpade && campo[dragon.getX()][dragon.getY()] != Maze.cDragonSpadeSleeping)
							campo[dragon.getX()][dragon.getY()] = Maze.cSpace;
						else
							campo[dragon.getX()][dragon.getY()] = Maze.cSpade;

						dragon.setX(dragon.getX()+auxX);
						dragon.setY(dragon.getY()+auxY);

						if(campo[dragon.getX()][dragon.getY()] != Maze.cSpade && campo[dragon.getX()][dragon.getY()] != Maze.cDragonSpade && campo[dragon.getX()][dragon.getY()] != Maze.cDragonSpadeSleeping)
							campo[dragon.getX()][dragon.getY()] = Maze.cDragon;
						else
						{
							if(dragon.isSleepy())
							{
								campo[dragon.getX()][dragon.getY()] = Maze.cDragonSpadeSleeping;
							}
							else
							{
								campo[dragon.getX()][dragon.getY()] = Maze.cDragonSpade;
							}
						}

					}
				}

				if(gameMode == 2) {
					
					int tmp = 1;
					if(auxS == 1) {
						tmp = 0;
					} else if(auxS == -1){
						tmp = r.nextInt(4);
					} 
					if(tmp == 0 && dragon.isDead()==false) {
						dragon.setSleepy(true);
						
						if(dragon.getX() == spade.getX() && dragon.getY() == spade.getY())
							campo[dragon.getX()][dragon.getY()] = Maze.cDragonSpadeSleeping;
						else
							campo[dragon.getX()][dragon.getY()] = Maze.cDragonSleeping;

					} else if (dragon.isDead()==false) {
						dragon.setSleepy(false);
						if(dragon.getX() == spade.getX() && dragon.getY() == spade.getY())
							campo[dragon.getX()][dragon.getY()] = Maze.cDragonSpade;
						else
							campo[dragon.getX()][dragon.getY()] = Maze.cDragon;
					}
				}
			}
		}
	}

	private void moveEagle() {

		// Apaga posi��o anterior
		campoAguia[eagle.getX()][eagle.getY()] = ' ';

		if(spade.isCollected())
			return;

		// Se n�o tiver sido lan�ada, acompanhar heroi
		if(!eagle.isLaunched()) {
			eagle.setX(hero.getX());
			eagle.setY(hero.getY());
		} else {
			int Xdesejado;
			int Ydesejado;
			eagle.setFlying(true);

			if(!spade.isEagleCollected()) {
				Xdesejado = spade.getX(); Ydesejado = spade.getY();
			} else {
				Xdesejado = eagle.getStartX(); Ydesejado = eagle.getStartY();
			}

			// Em X				
			if(Xdesejado != eagle.getX())
				eagle.setX( eagle.getX() + (Xdesejado - eagle.getX()) / Math.abs(Xdesejado - eagle.getX()));

			// Em Y
			if(Ydesejado != eagle.getY())
				eagle.setY( eagle.getY() + (Ydesejado - eagle.getY()) / Math.abs(Ydesejado - eagle.getY()));

			if(Xdesejado == eagle.getX() && Ydesejado == eagle.getY()) {
				// Chegou
				if(spade.isEagleCollected()) {
					eagle.setFlying(false);
					eagle.setEnd(true);
					spade.setEagleCollected(false);
					spade.setX(eagle.getX());
					spade.setY(eagle.getY());

					// Se Heroi estava l� � espera
					if(hero.getX() == eagle.getX() && hero.getY() == eagle.getY()) {
						spade.setCollected(true);
						campo[hero.getX()][hero.getY()] = Maze.cHeroArmed;
					} else {
						if(campo[eagle.getX()][eagle.getY()] == ' ')
							campo[eagle.getX()][eagle.getY()] = Maze.cSpade;
					}

					campoAguia[eagle.getX()][eagle.getY()] = ' ';

				} else {
					spade.setEagleCollected(true);
					eagle.setFlying(false);
				}
			} else {
				if(spade.isEagleCollected() && campo[spade.getX()][spade.getY()] == Maze.cSpade)
					campo[spade.getX()][spade.getY()] = ' ';
			}
		}

		if(!eagle.isDead())
			campoAguia[eagle.getX()][eagle.getY()] = Maze.cEagle;
	}

	private boolean moveHero(char c) 
	{

		int x1=hero.getX();
		int y1=hero.getY();

		if (c=='W' && campo[hero.getX()-1][hero.getY()]!=Maze.cWall && (campo[hero.getX()-1][hero.getY()]!='S' || spade.isCollected()))
			hero.setX(hero.getX()-1);
		else if(c=='A' && campo[hero.getX()][hero.getY()-1]!=Maze.cWall && (campo[hero.getX()][hero.getY()-1]!='S' || spade.isCollected()))
			hero.setY(hero.getY()-1);
		else if (c=='S' && campo[hero.getX()+1][hero.getY()]!=Maze.cWall && (campo[hero.getX()+1][hero.getY()]!='S' || spade.isCollected()))
			hero.setX(hero.getX()+1);
		else if (c=='D' && campo[hero.getX()][hero.getY()+1]!=Maze.cWall && (campo[hero.getX()][hero.getY()+1]!='S' || spade.isCollected()))
			hero.setY(hero.getY()+1);
		else
			throw new IllegalArgumentException();


		campo[x1][y1]=' ';

		if(spade.isCollected())
		{
			if(campo[hero.getX()][hero.getY()] == Maze.cExit)
			{
				
				boolean allDragonsDead = true;
				for(Dragon dragon : dragons) {
					if(!dragon.isDead()) {
						allDragonsDead = false;
						break;
					}
				}
				
				if(allDragonsDead)
					return false;
				
				campo[x1][y1] = Maze.cHeroArmed;
				
				hero.setX(x1);
				hero.setY(y1);
			} else {
				campo[hero.getX()][hero.getY()] = Maze.cHeroArmed;
			}

		}
		else
			campo[hero.getX()][hero.getY()] = Maze.cHero;

		return true;
	}

	/**
	 * Fun��o chamada para cada jogada
	 * 
	 * @param heroDirection
	 * @param eagleHelp
	 * @return false em caso de erro ou caso acabe o jogo, true em caso contr�rio
	 */
	public boolean play(char heroDirection, int eagleHelp) {
		return play(heroDirection, eagleHelp, -2, -2, -1);
	}
	
	/**
	 * Fun��o chamada para cada jogada, com atributos para permitir o uso de JUnit Tests
	 * 
	 * @param heroDirection
	 * @param eagleHelp
	 * @param dragY
	 * @param dragX
	 * @param dragS
	 * @return false em caso de erro ou caso acabe o jogo, true em caso contr�rio
	 */
	public boolean play(char heroDirection, int eagleHelp, int dragY, int dragX, int dragS) {

		// DEPRECATED
		// Perguntar movimento do Her�i - com verifica��o
		//askHeroDirection();
		
		// Mover �guia
		if(!eagle.isLaunched() && eagleHelp == 1) {
			eagle.setLaunched(true);
			eagle.setStartX(hero.getX());
			eagle.setStartY(hero.getY());			
		}				
				
		// Mover Heroi
		try {
			if(!moveHero(heroDirection))
				return false;
		} catch(IllegalArgumentException e) {
			throw e;
		}

		// NOTA: Mudou-se a ordem do moveHero e checkSpade. Verificar se h� erros
		// Verificar se apanhou a Espada
		if(!spade.isCollected() && !spade.isEagleCollected())
			checkSpade();

		// DEPRECATED
		// Perguntar se quer lan�ar a �guia
		//if(!eagle.isLaunched() && !eagle.isEnd() && !spade.isCollected())
		//	askEagle();

		

		if(!eagle.isEnd() && !spade.isCollected())
			moveEagle();
		
		if(eagle.isEnd() || spade.isCollected() || eagle.isDead())
			campoAguia[eagle.getX()][eagle.getY()] = ' ';


		// Verificar Mortes - se h� drag�es junto � Aguia ou ao Heroi
		if(!checkDeaths())
			return false;

		// Mover Drag�es

		moveDragons(dragY, dragX, dragS);

		// Verificar Mortes - se h� drag�es junto � Aguia ou ao Heroi
		if(!checkDeaths())
			return false;
		
		if(eagle.isEnd() || spade.isCollected() || eagle.isDead())
			campoAguia[eagle.getX()][eagle.getY()] = ' ';

		return true;	
	}

	
	/**
	 * Fun��o que reconhece
	 * 
	 * @return false se ocorrer um erro, true em caso contr�rio 
	 */
	public boolean recognizeElements() {

		List<Dragon> dragonsTmp = new ArrayList<Dragon>();

		boolean foundHero = false, foundSpade = false, foundDragon = false;

		for(int i = 0; i < campo.length; i++) {
			for(int j = 0; j < campo[i].length; j++) {
				if(campo[i][j] == Maze.cHero) {
					hero.setX(i);
					hero.setY(j);
					eagle.setX(i);
					eagle.setY(j);

					campoAguia = new char[campo.length][campo.length];
					for(int k = 0; k < campo.length; k++)
						for(int l = 0; l < campo.length; l++)
							campoAguia[k][l] = ' ';
					campoAguia[i][j] = Maze.cEagle;						

					foundHero = true;
				} else if(campo[i][j] == Maze.cSpade) {
					spade.setX(i);
					spade.setY(j);
					foundSpade = true;
				} else if(campo[i][j] == Maze.cDragon) {
					Dragon tmp = new Dragon(i, j);
					dragonsTmp.add(tmp);
					foundDragon = true;
				}
			}
		}

		dragons = dragonsTmp.toArray(new Dragon[0]);
		return (foundHero && foundSpade && foundDragon);

		//throw 

	}

	private void removeDeadDragons() {
		for(Dragon dragon : dragons) {
			if(dragon.isDead()) {
				if(campo[dragon.getX()][dragon.getY()] == Maze.cDragon)
					campo[dragon.getX()][dragon.getY()] = ' ';
			}
		}
	}

	/**
	 * Fun��o que salva o jogo para ficheiro
	 * 
	 * @return false em caso de erro, true em caso contr�rio
	 */
	public boolean save() {
		try{

			ObjectOutputStream save = new ObjectOutputStream(new FileOutputStream("saveFile.sav"));

			save.writeObject(gameMode);
			save.writeObject(campo);
			save.writeObject(campoAguia);			
			save.writeObject(hero);			
			save.writeObject(spade);			
			save.writeObject(eagle);			
			save.writeObject(dragons);

			save.close();

		} catch(Exception e) {
			System.err.println("Error: " + e.getMessage());
			return false;
		}

		return true;

	}

	/**
	 * Set da vari�vel campoAguia
	 * 
	 * @param campoAguia
	 */
	public void setCampoAguia(char[][] campoAguia) {
		this.campoAguia = campoAguia;
	}

	/**
	 * Set da vari�vel Eagle
	 * 
	 * @param eagle
	 */
	public void setEagle(Eagle eagle) {
		this.eagle = eagle;
	}

	/**
	 * Set da vari�vel Hero
	 * 
	 * @param hero
	 */
	public void setHero(Hero hero) {
		this.hero = hero;
	}

	/**
	 * Set da vari�vel Espada
	 * 
	 * @param spade
	 */
	public void setSpade(Spade spade) {
		this.spade = spade;
	}


}
