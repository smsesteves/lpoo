package maze.logic;

import java.io.Serializable;

/**
 * 
 * Classe que representa a entidade Aguia
 * 
 * @author Andre Duarte
 * @author Sergio Esteves
 * @see Serializable
 *
 */
public class Eagle extends GameElement implements Serializable {



	private static final long serialVersionUID = 1L;


	private boolean flying = false;
	private int startX;
	private int startY;
	private boolean launched = false;
	private boolean dead = false;
	private boolean end = false;


	/**
	 * Construtor
	 * 
	 * @param X
	 * @param Y
	 */
	public Eagle(int X, int Y) {
		super(X, Y);
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 * Construtor por defeito
	 * 
	 */
	public Eagle() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * 
	 * Get da variavel end
	 * 
	 * @return true se a missao aguia no jogo ja terminou
	 */
	public boolean isEnd() {
		return end;
	}
	/**
	 * 
	 * Set da variavel end
	 * 
	 * @param end true se a missao da aguia ja terminou e falso para o caso contrario
	 */
	public void setEnd(boolean end) {
		this.end = end;
	}

	/**
	 * 
	 * Get da variavel flying
	 * 
	 * @return true se a aguia esta a voar e falso para quando esta com o heroi ou quando vai apanhar a espada
	 */
	public boolean isFlying() {
		return flying;
	}

	/**
	 * 
	 * Set da variavel flying
	 * 
	 * @param flying true se a aguia esta a voar e falso para quando esta com o heroi ou quando vai apanhar a espada
	 */
	public void setFlying(boolean flying) {
		this.flying = flying;
	}

	/**
	 * 
	 * Retorna a posicao X inicial da aguia
	 * 
	 * @return posicao inicial X da aguia
	 */
	public int getStartX() {
		return startX;
	}
	/**
	 * 
	 * Set da variavel startX
	 * 
	 * @param startX posicao inciial X da aguia
	 */
	public void setStartX(int startX) {
		this.startX = startX;
	}
	/**
	 * 
	 * Retorna a posicao Y inicial da aguia
	 * 
	 * @return posicao inicial Y da aguia
	 */
	public int getStartY() {
		return startY;
	}
	/**
	 * 
	 * Set da variavel startY
	 * 
	 * @param startY
	 */
	public void setStartY(int startY) {
		this.startY = startY;
	}


	/**
	 * 
	 * Get da variavel launched
	 * 
	 * @return true se a aguia ja foi lancada e falso se aidna continua com o heroi
	 */
	public boolean isLaunched() {
		return launched;
	}

	/**
	 * 
	 * Set da variavel launched
	 * 
	 * @param launched true se a aguia ja foi lancada e falso se aidna continua com o heroi
	 */
	public void setLaunched(boolean launched) {
		this.launched = launched;
	}


	/**
	 * 
	 * Get da variavel dead
	 * 
	 * @return true se a aguia morreu e falso se a aguia esta viva
	 */
	public boolean isDead() {
		return dead;
	}

	/**
	 * 
	 * Set da variavel deadd
	 * 
	 * @param dead true se a aguia morreu e falso se a aguia esta viva
	 */
	public void setDead(boolean dead) {
		this.dead = dead;
	}

}
