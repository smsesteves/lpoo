package com.lpoo.teeter.models;

/**
 * The type Coord.
 */
public class Coord {



    Coord(int Xi,int Xf,int Yi,int Yf)
    {
        xi=Xi;
        xf=Xf;
        yi=Yi;
        yf=Yf;
    }
    private int xi;
    private int xf;
    private int yi;
    private int yf;


    /**
     * Gets xi.
     *
     * @return the xi
     */
    public int getXi() {
        return xi;
    }


    /**
     * Sets xi.
     *
     * @param xi the xi
     */
    public void setXi(int xi) {
        this.xi = xi;
    }

    /**
     * Gets xf.
     *
     * @return the xf
     */
    public int getXf() {
        return xf;
    }


    /**
     * Sets xf.
     *
     * @param xf the xf
     */
    public void setXf(int xf) {
        this.xf = xf;
    }


    /**
     * Gets yi.
     *
     * @return the yi
     */
    public int getYi() {
        return yi;
    }


    /**
     * Sets yi.
     *
     * @param yi the yi
     */
    public void setYi(int yi) {
        this.yi = yi;
    }

    /**
     * Gets yf.
     *
     * @return the yf
     */
    public int getYf() {
        return yf;
    }

    /**
     * Sets yf.
     *
     * @param yf the yf
     */
    public void setYf(int yf) {
        this.yf = yf;
    }

}