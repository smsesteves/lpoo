package maze.logic;

import java.io.Serializable;

/**
 * 
 * Classe representativa da interface Heroi
 * 
 * @author Andre Duarte 
 * @author Sergio Esteves 
 * @see Serializable
 *
 */
public class Hero extends GameElement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
