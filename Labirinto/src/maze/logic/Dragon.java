package maze.logic;

import java.io.Serializable;





/**
 * 
 * Classe responsavel por representar a Entidade Dragao
 * 
 * @author Andre Duarte 
 * @author Sergio Esteves 
 * @see  Serializable
 *
 */
public class Dragon extends GameElement implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private boolean dead = false;
	private boolean sleepy = false;
	
	
	/**
	 * Construtor
	 *  
	 * @param X
	 * @param Y
	 */
	public Dragon(int X, int Y) {
		super(X, Y);
	}
	
	
	/**
	 * 
	 * Funcao que retorna a variv�vel dead
	 * 
	 * @return true se o Dragao morreu e falso se encontra vivo
	 */
	public boolean isDead() {
		return dead;
	}
	
	/**
	 * 
	 * Set da variavel dead
	 * 
	 * @param dead boolean que representa se um dragao est� morto ou vivo
	 */
	public void setDead(boolean dead) {
		this.dead = dead;
	}
	
	
	/**
	 * 
	 * Get variavel sleepy
	 * 
	 * @return true se o Dragao est� adormecido e falso se est� ativo
	 */
	public boolean isSleepy() {
		return sleepy;
	}
	
	
	/**
	 * 
	 * Set da Variavel sleepy
	 * 
	 * @param sleepy boolean que representa se um dragao est� adormecido
	 */
	public void setSleepy(boolean sleepy) {
		this.sleepy = sleepy;
	}
	
	
	/**
	 * 
	 * Funcao que devolve true se um dragao est� em movimento ou n�o
	 * 
	 * @return true se o dragao pode movimentar-se e falso para o casso contrario
	 */
	
	public boolean isMoving() {
		return (!dead && !sleepy);			
	}

}
