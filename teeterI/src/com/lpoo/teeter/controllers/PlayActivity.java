package com.lpoo.teeter.controllers;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.WindowManager;

import com.lpoo.teeter.R;
import com.lpoo.teeter.core.*;
import com.lpoo.teeter.models.Files;
import com.lpoo.teeter.models.Game;
import com.lpoo.teeter.views.PlayView;
import org.dyn4j.geometry.*;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * The type Play activity.
 */
public class PlayActivity extends FullscreenActivity implements SensorEventListener {

    private PlayView playView;
    private SensorManager mSensorManager;
    private Sensor mGravity;

    Game game = null;

    double ax = 0, ay = 0;

    Thread displayThread = null;

    boolean toRest;

    int level_number = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Screen Timeout Disable
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // View
        playView = new PlayView(this);
        setContentView(playView);

        // Background
        getWindow().getDecorView().setBackgroundColor(Color.BLACK);

        // Sensores
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        // Nivel
        if(getIntent().getExtras() != null && getIntent().getExtras().getString("LEVEL_ID").trim() != "")
            level_number = Integer.parseInt(getIntent().getExtras().getString("LEVEL_ID"));
        else
            level_number = 10;

        initGame();

        toRest = false;

        displayThread = new Thread(new DisplayRefresher());
        displayThread.start();
    }

    private void initGame() {
        int resID = getResources().getIdentifier(   "level_" + String.valueOf(level_number),
                                                    "raw", getPackageName());

        try {
            InputStream is = getResources().openRawResource(resID);
            game = new Game(is);
        } catch (Exception e) {  }

        playView.setGame(game);
    }

    int k = 0;
    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mGravity, SensorManager.SENSOR_DELAY_NORMAL);

        if(toRest)
            game.setRest(true);
    }

    @Override
    protected void onPause() {
        super.onPause();

        toRest = true;
        game.getWorld().getBody(0).setVelocity(new Vector2(0,0));
    }

    @Override
    public boolean onTouchEvent(final MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_UP) {
                if(game.isRest()) {
                    if(game.isLose()) {
                        game.resetBall();
                        game.setLose(false);
                    } else if(game.isWon()) {
                        game.setWon(false);
                        initGame();
                    }
                    game.setRest(false);
                } else {
                    game.setRest(true);
                    game.getWorld().getBody(0).setVelocity(new Vector2(0,0));
                }
        }

        return true;
    }


    class DisplayRefresher implements Runnable {

        // @Override
        public void run() {
            // initialize the last update time
            long last = System.nanoTime();

            while(!displayThread.isInterrupted()) {
                try {
                    displayThread.sleep(1000/Settings.get().FPS);
                } catch (InterruptedException e) {
                    displayThread.interrupt();
                }

                if(!game.isRest()) {
                    game.getWorld().getBody(0).applyForce(new Vector2(ax,ay));
                    game.getWorld().update((System.nanoTime() - last) * 10E9);

                    detectFalls();
                }

                playView.postInvalidate();

                last = System.nanoTime();
            }
        }
    }

    private void detectFalls() {
        ArrayList<Circle> holes = game.getHoles();

        Vector2 ballPosTmp = new Vector2(   game.getWorld().getBody(0).getTransform().getTranslationX()*10,
                                            game.getWorld().getBody(0).getTransform().getTranslationY()*10);

        for(int i = 0; i < holes.size(); i++)
            if(distance2d(holes.get(i).getCenter(), ballPosTmp) < Settings.get().GRID_SIZE/2) {
                game.setRest(true);
                game.setLose(true);
                break;
            }

        // Final Hole
        Circle finalHole = game.getFinalHole();
        if(distance2d(finalHole.getCenter(), ballPosTmp) < Settings.get().GRID_SIZE/2) {
            if(unlockedLevel < 9) {
                level_number++;
                if(level_number >= unlockedLevel) {
                    unlockedLevel = level_number;
                    Files.get().writeToSDFile(String.valueOf(unlockedLevel));
                }
            }

            game.setRest(true);
            game.setWon(true);
        }
    }

    private static double distance2d(Vector2 a, Vector2 b)
    {
        double dx = a.x - b.x;
        double dy = a.y - b.y;
        return Math.sqrt(dx * dx + dy * dy);
    }


    @Override
    public void onSensorChanged(SensorEvent e) {
        if(level_number != 10)
            sensorChanged(e.values[1], e.values[0]);
    }

    public void sensorChanged(float x, float y) {
        if(x > Settings.get().BALL_THRESHOLD || x < (-1)*Settings.get().BALL_THRESHOLD)
            ax = x*200;

        if(y > Settings.get().BALL_THRESHOLD || y < (-1)*Settings.get().BALL_THRESHOLD)
            ay = y*200;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public Game getGame() {
        return game;
    }


}
