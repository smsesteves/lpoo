package com.lpoo.teeter.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lpoo.teeter.R;
import com.lpoo.teeter.core.Settings;
import com.lpoo.teeter.models.Coord;
import com.lpoo.teeter.models.Game;
import org.dyn4j.geometry.Circle;

import java.util.ArrayList;


/**
 * The type Play view.
 */
public class PlayView extends View {

    Drawable background = null;
    Drawable wall = null;
    Drawable ball = null;
    Drawable hole = null;
    Drawable finalHole = null;

    Paint textPaint = null;

    Game game = null;

    /**
     * Instantiates a new Play view.
     *
     * @param context the context
     */
    public PlayView(Context context) {
        super(context);

        // Inicializar imagens
        background = getResources().getDrawable(R.drawable.play_background);
        wall = getResources().getDrawable(R.drawable.wall);
        ball = getResources().getDrawable(R.drawable.ball);
        hole = getResources().getDrawable(R.drawable.hole);
        finalHole = getResources().getDrawable(R.drawable.final_hole);

        textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(35);
        textPaint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // Fundo
        background.setBounds(0,0,getRight(),getBottom());
        background.draw(canvas);

        int x, y;

        // Walls
        ArrayList<Coord> walls = game.getWalls();
        for(int i = 0; i < walls.size(); i++) {
            Coord a = walls.get(i);
            wall.setBounds(a.getXi(), a.getYi(), a.getXf(), a.getYf());
            wall.draw(canvas);
        }

        // Holes
        ArrayList<Circle> holes =  game.getHoles();
        for(int i = 0; i < holes.size(); i++)
            drawCircle( hole, canvas,
                        (int)holes.get(i).getCenter().x,
                        (int)holes.get(i).getCenter().y);

        // Final Hole

        drawCircle( finalHole, canvas,
                    (int)game.getFinalHole().getCenter().x,
                    (int)game.getFinalHole().getCenter().y);

        // Bola
        drawCircle( ball, canvas,
                    (int)(game.getWorld().getBody(0).getTransform().getTranslationX()*10),
                    (int)(game.getWorld().getBody(0).getTransform().getTranslationY()*10));

        // Pausado
        if(game.isRest()) {
            // Overlay
            wall.setBounds(0, 0, Settings.get().WIDTH, Settings.get().HEIGHT);
            wall.setAlpha(950);
            wall.draw(canvas);
            wall.setAlpha(1000);

            // Texto
            String text = "Paused";
            if(game.isLose())
                text = "You lost. Try again.";
            else if(game.isWon())
                text = "You Won. Play next.";

            canvas.drawText(text,
                            (canvas.getWidth() / 2),
                            (int)((canvas.getHeight() / 2) - ((textPaint.descent() + textPaint.ascent()) / 2)),
                            textPaint);
        }
    }

    private void drawCircle(Drawable obj, Canvas canvas, int x, int y) {

        obj.setBounds( x - (Settings.get().GRID_SIZE/2), y - (Settings.get().GRID_SIZE/2),
                        x + (Settings.get().GRID_SIZE/2), y + (Settings.get().GRID_SIZE/2));
        obj.draw(canvas);
    }

    /**
     * Sets game.
     *
     * @param game the game
     */
    public void setGame(Game game) {
        this.game = game;
    }

}
