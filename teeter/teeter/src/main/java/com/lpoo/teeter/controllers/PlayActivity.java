package com.lpoo.teeter.controllers;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.WindowManager;

import com.lpoo.teeter.R;
import com.lpoo.teeter.models.Game;
import com.lpoo.teeter.views.PlayView;
import com.lpoo.teeter.core.FullscreenActivity;

import java.io.InputStream;

public class PlayActivity extends FullscreenActivity implements SensorEventListener {

    private PlayView playView;
    private SensorManager mSensorManager;
    private Sensor mGravity;

    final double ballThreshold = 0.3, ballSpeed = 9;

    Game game = null;

    Boolean rest = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Screen Timeout Disable
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // View
        playView = new PlayView(this);
        setContentView(playView);

        getWindow().getDecorView().setBackgroundColor(Color.BLACK);

        // Sensores
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        // Criação do Jogo
        try {
            InputStream is = getResources().openRawResource(R.raw.level_1);
            game = new Game(is);
            game.ball.setFps(60);
        } catch (Exception e) {  }

        // Copiar campo
        for(int i = 0; i < 18; i++)
            for(int j = 0; j < 32; j++)
                playView.campo[i][j] = game.campo[i][j];

        new Thread(new DisplayRefresher()).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mGravity, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public boolean onTouchEvent(final MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_UP) {
                if(rest)
                    rest = false;
                else {
                    rest = true;
                    game.ball.reset();
                }
        }

        return true;
    }

    @Override
    public void onSensorChanged(SensorEvent e) {
        if(e.values[0] > ballThreshold || e.values[0] < (-1)*ballThreshold)
            game.ball.setAx(e.values[1]);

        if(e.values[1] > ballThreshold || e.values[1] < (-1)*ballThreshold)
            game.ball.setAy(e.values[0]);
    }

    class DisplayRefresher implements Runnable {
        // @Override
        public void run() {
            while(!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(1000/60);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                if(!rest) {
                    game.ball.move(game.walls);
                    playView.setBallX(game.ball.getX());
                    playView.setBallY(game.ball.getY());
                }

                playView.setRest(rest);

                playView.postInvalidate();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


}
